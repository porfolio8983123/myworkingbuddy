const workout = require('../models/workouts');
const mongoose = require('mongoose');

const createworkout = async (req,res) => {
    const {title,load,reps} = req.body;

    let emptyField = [];

    if (!title) {
        emptyField.push('title');
    }

    if (!load) {
        emptyField.push('load');
    }

    if (!reps) {
        emptyField.push('reps')
    }

    if (emptyField.length > 0) {
        return res.status(400).json({error:'Please fill in all the fields'});
    }

    try {
        const work = await workout.create({
            title,
            load,
            reps
        })
        res.status(200).json(work);
    } catch (error) {
        res.status(400).json({error: error.message});
    }
}

const getWorkouts = async (req,res) => {
    const works = await workout.find({}).sort({createdAt:-1})
    res.status(200).json(works)
}

const getSingleWorkouts = async (req,res) => {
    const {id} = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({error: 'No such workout'});
    }

    const work = await workout.findById(id)
    if (!work) {
        return res.status(404).json({error:"No such workout"});
    }
    res.status(200).json(work);
}

const deleteWorkout = async (req,res) => {
    const {id} = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({error: 'No such workout'}); 
    }

    const work = await workout.findOneAndDelete({_id:id});
    if (!work) {
        return res.status(404).json({error: "No such work"})
    }
    res.status(200).json(work);
}

const updateWorkout = async (req,res) => {
    const {id} = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({error: 'No such workout'}); 
    }

    const work = await workout.findOneAndUpdate({_id: id},{
        ...req.body
    })

    if (!work) {
        return res.status(404).json({error: "No such work"})
    }
    res.status(200).json(work);
}

module.exports = {
    createworkout,
    getWorkouts,
    getSingleWorkouts,
    deleteWorkout,
    updateWorkout
}