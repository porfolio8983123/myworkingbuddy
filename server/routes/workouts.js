const express = require('express');
const router = express.Router();
const {createworkout, getWorkouts, getSingleWorkouts, deleteWorkout,updateWorkout} = require('../controllers/workoutController')

router.get('/', getWorkouts)

router.get("/:id",getSingleWorkouts)

router.post("/",createworkout)

router.delete("/:id",deleteWorkout)

router.patch("/:id", updateWorkout)

module.exports = router;