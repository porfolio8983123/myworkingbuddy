const express = require('express');
const cors = require('cors');
require('dotenv').config();
const mongoose = require('mongoose');
const workoutRoutes = require('./routes/workouts');

const app = express();

app.use(express.json());
app.use(cors());

app.use("/api/workouts",workoutRoutes)

mongoose.connect(process.env.MONGO_URI)
.then(() => {
    app.listen(process.env.PORT,() => {
        console.log("Listening on port 5000")
    })
}).catch((error) => {
    console.log(error.message);
})