import { useEffect } from 'react'
import WorkoutDetials from '../components/WorkoutDetials';
import WorkoutForm from '../components/WorkoutForm';
import { useWorkoutsContext } from '../components/hooks/WorkoutContext';
import { workoutDomain } from '../domain';

function Home() {

    const {workouts,dispatch}  = useWorkoutsContext();

    useEffect(() => {
        const fetchWorkout = async () => {
            const response = await fetch(`${workoutDomain}/api/workouts`)
            const parseRes = await response.json();

            if (response.ok) {
                dispatch({type: 'SET_WORKOUTS', payload:parseRes})
            }
        }

        fetchWorkout();
    },[dispatch]);

    return (
        <div className='home'>
            <div className='workouts'>
                {workouts && workouts.map((workout) => (
                    <WorkoutDetials key={workout._id} workout = {workout}/>
                ))}
            </div>
            <WorkoutForm/>
        </div>
    )
}

export default Home