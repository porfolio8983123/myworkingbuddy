import React from 'react'
import { useWorkoutsContext } from './hooks/WorkoutContext';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { workoutDomain } from '../domain';

function WorkoutDetials({workout}) {
    const {dispatch} = useWorkoutsContext();

    const handleClick = async () => {
        const response = await fetch(`${workoutDomain}/api/workouts/${workout._id}`,{
            method:'Delete'
        })
        const parseRes = await response.json();

        if (response.ok) {
            dispatch({type:'DELETE_WORKOUT',payload:parseRes})
        }
    }
  return (
    <div className='workout-details'>
        <h4>{workout.title}</h4>
        <p><strong>Load (kg): </strong>{workout.load}</p>
        <p><strong>Reps: </strong>{workout.reps}</p>
        <p>{formatDistanceToNow(new Date(workout.createdAt),{addSuffix:true})}</p>
        <span onClick={handleClick}>Delete</span>
    </div>
  )
}

export default WorkoutDetials